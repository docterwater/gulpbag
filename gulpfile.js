var gulp = require('gulp'),             // 載入 gulp
    gulpSass = require('gulp-sass');    // 載入 gulp-sass
    gulpMinifycss = require('gulp-minify-css');
    gulpUglify = require('gulp-uglify');  // 載入 gulp-uglify
    autoprefixer = require('gulp-autoprefixer');
    gulpConcat = require('gulp-concat'); //合併
    gulpRename = require('gulp-rename');

    
gulp.task('ok',async()=>{
    await gulp.series('styles', 'call','watch');
} );
  

gulp.task('styles', async()=> {
    await gulp.src('scss/**/*.scss')    // 指定要處理的 Scss 檔案目錄
        .pipe(gulpSass({outputStyle: 'expanded'}).on('error', gulpSass.logError))         // 編譯 Scss
        .pipe(autoprefixer({
            cascade: false,
        }))
        .pipe(gulpConcat('concat.css'))
        .pipe(gulp.dest('css/css'))  // 指定最小化後的 JavaScript 檔案目錄
        .pipe(gulpRename({suffix: '.min'}))   //rename壓縮後的檔名
        .pipe(gulpMinifycss())   //執行壓縮
        .pipe(gulp.dest('css/minifyCss'));  // 指定編譯後的 css 檔案目錄
});
// 定義名稱為 default 的 gulp 工作
gulp.task('call', async()=> {
    await console.log('Hello Gulp Default Task');
});

gulp.task('watch', function () {
    gulp.watch('javascript/original/*.js', gulp.series('script'));
    gulp.watch('scss/**/*.scss', gulp.series('styles'));
});

gulp.task('script', done => {
    gulp.src('javascript/original/*.js')        // 指定要處理的原始 JavaScript 檔案目錄
        .pipe(gulpConcat('main.js'))    //合併所有js到main.js
        .pipe(gulp.dest('javascript/js'))  // 指定最小化後的 JavaScript 檔案目錄
        .pipe(gulpRename({suffix: '.min'}))   //rename壓縮後的檔名
        .pipe(gulpUglify())    //壓縮
        .pipe(gulp.dest('javascript/minify'));  //輸出
        done();
});
